import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public final class Main {
	private BufferedReader reader;

    private String input;
	
	public static void main(String[] args) throws IOException {
		Main main = new Main();
		main.run(); //, baby
	}
	
	public Main(){
		reader = new BufferedReader(new InputStreamReader(System.in));
	}

    public final void run() throws IOException {
        while(readInput()){
            //Your code goes here
        }
    }

    private final boolean readInput() throws IOException {
		if((input = reader.readLine()) != null) {
            System.out.println(input);
            return true;
        }
        return false;
    }

}
