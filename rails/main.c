#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_SIZE 7000
#define SIZE 10


void read_input( char *input, int **coaches, int N ) {
    char *token = NULL;
    int *cs = *coaches;

    token = strtok( input, " " );
    int i = N - 1;
    while(token != NULL ){// the 
        cs[i] = atoi( token );
        i--;
        token = strtok( NULL, " " );
    }

    if( !cs[N - 1] ) {

        free( *coaches);
        *coaches = NULL;
    }
}

int read_total_coaches() {
    char total_coaches[SIZE];
    
    fgets(total_coaches, SIZE, stdin);
   
    return  atoi( total_coaches );
}

int is_valid_permutation(int * e, int N) {
    int i = 0;
    int j = 1;
    int Max = N;
    int last = 0;

    for(; j < N ; i++,j++){
        if(e[i] == Max){// descarta
            Max--;
            if(last && last == Max) {
                Max--;
            }
        }else if(e[i] > e[j] && e[i] < Max){
            return 0;
        }else{
            last = e[i];
        }
    }

    return 1;
}

int main() {
    int N = 0;
    char line[MAX_SIZE];

    N = read_total_coaches();
    int *coaches = malloc( N * sizeof(int) );
    while(N != 0) {
        memset(line, 0, MAX_SIZE);
        fgets(line, MAX_SIZE , stdin) ; 
                    
        read_input(line, &coaches, N);

        if(coaches){
            
            if( is_valid_permutation( coaches, N ) ) {
                printf("Yes\n");
            }else{
                printf("No\n");
            }
        }else {
            printf("\n");
            N = read_total_coaches();
            free(coaches);
            coaches = malloc( N * sizeof(int) );
        }
    }
    

    return 0;
}
