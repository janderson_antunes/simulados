import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.ListIterator;
import java.util.Scanner;



public final class Main {
	//private Scanner scanIn;
	private BufferedReader reader;
	private int parkLen;
	private int emptySpace;
	private int totalEvent;
	public int incomings;
	private ArrayList<Block> blocks;
	private Block parkedCars[] = new Block[10000]; // forget zero index...
	 
	
	
	public static void main(String[] args) throws NumberFormatException, IOException {
		Main main = new Main();
		
		while(main.readTotals()){
			//System.out.println(main);
			main.readEvent();
			System.out.println(main.incomings);
		}
		
	}
	
	public Main(){
		reader = new BufferedReader(new InputStreamReader(System.in));
	}
	
	public final boolean readTotals() throws NumberFormatException, IOException{
		parkLen = totalEvent = incomings = emptySpace = 0;
		String line;
		if((line = reader.readLine()) != null) {
			this.cleanParked();
			String pars[] = line.split("\\s");
			parkLen = emptySpace =Integer.parseInt(pars[0]); 
			//blocks = new Block[]{new Block(0,parkLen)}; // super block
			blocks = new ArrayList<Main.Block>();
			blocks.add(new Block(0,parkLen));
			totalEvent = Integer.parseInt(pars[1]);
			
			return true;
		}
		return false;
	}
	public final void readEvent() throws IOException{
		for (int i = 0; i < totalEvent; i++) {
			String row = reader.readLine();
			proccessEvent(row);
		}
	}

	public String toString(){
		return String.format("<Len=%d, Event=%d, Empty=%d>", parkLen, totalEvent, emptySpace);
	}
	private final void cleanParked(){
		Arrays.fill(this.parkedCars, null);
	}
	
	private final void proccessEvent(String row){
		String pars[] = row.split("\\s");
		int car = Integer.parseInt(pars[1]);
		int carLen;
		
		if(pars[0].equals("C") && parkedCars[car] == null && emptySpace >= (carLen = Integer.parseInt(pars[2]))) {
			
			Block block = searchPark(car, carLen);
			if(block != null){
				
				//System.out.println(block);
				incomings += 10;
				emptySpace -= carLen;
				parkCarAt(block, car, carLen);
//				System.out.println("C: "+this);
			}
		}else if(parkedCars[car] != null){ // "S"

			Block block = parkedCars[car];
			block.filled = false;
			emptySpace += block.len;
			parkedCars[car] = null;
			int index = blocks.indexOf(block);
			
			if (index + 1 < blocks.size()) {
				// merge forward
				ListIterator<Block> lit = blocks.listIterator(index + 1);
				while (lit.hasNext()) {
					Block b = lit.next();
					if (b.filled) break; // endof game
					block.len += b.len;
					lit.remove();
				}
			}
			if(index <= 0) return; //enough...
			
			// merge backward
			ListIterator<Block> lit2 = blocks.listIterator(index );
			
			while(lit2.hasPrevious()){
				Block b = lit2.previous();
				if (b.filled) break; // endof game
				block.len += b.len; 
				lit2.remove();
			}
//			System.out.println("S: "+this);
		}
		
	}
	
	private final void parkCarAt(Block block, int car, int carLen) {
		int remain = block.len - carLen;
		block.filled = true;
		block.len = carLen;		
		if(remain > 0){
			Block newBlock = new Block(remain + block.offset, remain);
			blocks.add(blocks.indexOf(block) + 1, newBlock);
//			System.out.println(block + "/" + newBlock);
		}
		parkedCars[car] = block;
		
		
	}

	
	private final Block searchPark(int car, int carLen) {
		for (Block block : blocks) {
			if(! block.filled && block.len >= carLen) {
				return block;
			}
		}
//		for (int i = 0; i < blocks.length; i++) {
//			if(! blocks[i].filled ) {
//				return i;
//			}
//		}
		return null;
	}
	
	final class Block{
		public boolean filled;
		public int offset;
		public int len;

		public Block (int offset, int len){
			this.offset = offset;
			this.len = len;
			this.filled = false;
		}
		public Block (int offset, int len, boolean filled){
			this.offset = offset;
			this.len = len;
			this.filled = filled;
		}
		public String toString(){
			return String.format("Block<i=%d, l=%d, filled=%s>", offset, len, this.filled );
		}
	}

}
